//! `bzip2_rs` is a pure Rust bzip2 decoder.
//!
//! ## Main APIs
//!
//! * [`Decoder`]: low-level, no IO, bzip2 decoder
//! * [`DecoderReader`]: high-level synchronous bzip2 decoder
//!
//! ## Features
//!
//! * Default features: Rust >= 1.34.2 is supported
//! * `rustc_1_37`: bump MSRV to 1.37, enable more optimizations
//! * `nightly`: require Rust Nightly, enable more optimizations
//!
//! ## Usage
//!
//! ```rust,no_run
//! use std::fs::File;
//! use std::io;
//! use bzip2_rs::DecoderReader;
//!
//! # fn main() -> Result<(), Box<dyn std::error::Error>> {
//! let mut compressed_file = File::open("input.bz2")?;
//! let mut decompressed_output = File::create("output")?;
//!
//! let mut reader = DecoderReader::new(compressed_file);
//! io::copy(&mut reader, &mut decompressed_output)?;
//! # Ok(())
//! # }
//! ```
//!
//! [`Decoder`]: crate::decoder::Decoder

#![deny(
    trivial_casts,
    trivial_numeric_casts,
    rust_2018_idioms,
    clippy::cast_lossless,
    clippy::doc_markdown,
    missing_docs,
    broken_intra_doc_links
)]
#![forbid(unsafe_code)]
// TODO: remove once rustc 1.35 is our MSRV
#![allow(clippy::manual_range_contains)]

#[doc(no_inline)]
pub use self::decoder::DecoderReader;

mod bitreader;
pub mod block;
mod crc;
pub mod decoder;
pub mod header;
mod huffman;
mod move_to_front;

#[cfg(feature = "nightly")]
const LEN_258: usize = 258;
#[cfg(not(feature = "nightly"))]
const LEN_258: usize = 512;
