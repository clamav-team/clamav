Source: clamav
Section: utils
Priority: optional
Maintainer: ClamAV Team <pkg-clamav-devel@lists.alioth.debian.org>
Uploaders: Michael Meskes <meskes@debian.org>,
           Michael Tautschnig <mt@debian.org>,
           Scott Kitterman <scott@kitterman.com>,
           Sebastian Andrzej Siewior <sebastian@breakpoint.cc>,
           Andreas Cadhalpun <Andreas.Cadhalpun@googlemail.com>
Build-Depends: dpkg-dev (>= 1.22.5), automake,
               bindgen,
               cargo,
               check,
               cmake,
               debhelper-compat (= 12),
               dh-apparmor,
               dh-strip-nondeterminism,
               doxygen,
               libbz2-dev,
               libcurl4-openssl-dev,
               libjson-c-dev,
               libltdl-dev,
               libmilter-dev,
               libmspack-dev,
               libncurses-dev,
               libpcre2-dev,
               libssl-dev,
               libsystemd-dev [linux-any], systemd-dev,
               libxml2-dev,
               perl:native,
               pkg-config,
               po-debconf,
               python3-pytest,
               rust-gdb,
               rustfmt,
               systemd,
               zlib1g-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/clamav-team/clamav.git
Vcs-Browser: https://salsa.debian.org/clamav-team/clamav
Homepage: https://www.clamav.net/

Package: clamav-base
Architecture: all
Multi-Arch: foreign
Depends: adduser, logrotate, ucf, ${misc:Depends}
Recommends: clamav
Description: anti-virus utility for Unix - base package
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package mainly manages the clamav system account. It is not really
 useful without the clamav or clamav-daemon package.

Package: clamav-docs
Architecture: all
Multi-Arch: foreign
Section: oldlibs
Depends: ${misc:Depends}, clamav-doc
Description: anti-virus utility for Unix - documentation
 This package is a transitional package. The documentation has been moved to
 the clamav-doc package.

Package: clamav-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Breaks: clamav-docs (<< 1.2.1), clamav (<< 1.2.1)
Replaces: clamav-docs (<< 1.2.1), clamav (<< 1.2.1)
Description: anti-virus utility for Unix - documentation
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains the documentation for the ClamAV suite.

Package: clamav
Architecture: any
Multi-Arch: foreign
Depends: clamav-freshclam (>= ${source:Upstream-Version}) | clamav-data,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: clamav-base
Suggests: libclamunrar, clamav-doc
Description: anti-virus utility for Unix - command-line interface
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains the command line interface. Features:
  - built-in support for various archive formats, including Zip, Tar,
    Gzip, Bzip2, OLE2, Cabinet, CHM, BinHex, SIS and others;
  - built-in support for almost all mail file formats;
  - built-in support for ELF executables and Portable Executable files
    compressed with UPX, FSG, Petite, NsPack, wwpack32, MEW, Upack and
    obfuscated with SUE, Y0da Cryptor and others;
  - built-in support for popular document formats including Microsoft
    Office and Mac Office files, HTML, RTF and PDF.
 .
 For scanning to work, a virus database is needed. There are two options
 for getting it:
  - clamav-freshclam: updates the database from Internet. This is
    recommended with Internet access.
  - clamav-data: for users without Internet access. The package is
    not updated once installed. The clamav-getfiles package allows
    creating custom packages from an Internet-connected computer.

Package: libclamav-dev
Section: libdevel
Architecture: any
Depends: libbz2-dev,
         libc6-dev,
         libclamav12 (= ${binary:Version}),
         libssl-dev,
         zlib1g-dev,
         ${misc:Depends}
Description: anti-virus utility for Unix - development files
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 The package contains the needed headers and libraries for
 developing software using the libclamav interface.
 .
 This library can be used to develop virus scanner applications.

Package: libclamav12
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: libclamunrar, libclamunrar11
Breaks: libclamav12t64 (<< ${source:Version})
Replaces: libclamav12t64
Description: anti-virus utility for Unix - library
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 For programs written using the libclamav library. Libclamav may be used to add
 virus protection into software. The library is thread-safe, and automatically
 recognizes and scans archives. Scanning is very fast and most of the time
 not noticeable.

Package: clamav-daemon
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: adduser,
         clamav-base (= ${source:Version}),
         clamav-freshclam (>= ${source:Upstream-Version}) | clamav-data,
         dpkg (>= 1.16.1),
         procps (>= 1:3.3.2),
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: clamdscan
Suggests: libclamunrar, apparmor, clamav-doc, daemon [!hurd-any]
Description: anti-virus utility for Unix - scanner daemon
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains the daemon featuring:
  - fast, multi-threaded daemon;
  - easy integration with MTA's;
  - support for on-access scanning;
  - remote scanning;
  - able to be run supervised by daemon.

Package: clamdscan
Architecture: any
Depends: clamav-base (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: clamav-daemon, clamav-doc
Description: anti-virus utility for Unix - scanner client
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains clamdscan, the command line interface of the clamav
 daemon.

Package: clamav-testfiles
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: anti-virus utility for Unix - test files
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains files 'infected' with a test signature. The test
 signature (ClamAV-Test-Signature) should be detectable by all
 anti-virus programs.

Package: clamav-freshclam
Architecture: any
Conflicts: clamav-data, libclamav2, libclamav3
Provides: clamav-data
Suggests: apparmor, clamav-doc
Recommends: ca-certificates
Depends: clamav-base (>= ${source:Version}),
         dpkg (>= 1.16.1),
         logrotate,
         procps (>= 1:3.3.2),
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Description: anti-virus utility for Unix - virus database update utility
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains the freshclam program and scripts to automate virus
 database updating. It relies on an Internet connection, but can be
 run in a variety of ways to compensate for intermittent connections.

Package: clamav-milter
Architecture: any
Suggests: clamav-doc, daemon [!hurd-any]
Recommends: clamav-daemon
Pre-Depends: ${misc:Pre-Depends}
Depends: adduser,
         clamav-base (>= ${source:Version}),
         clamav-freshclam (>= ${source:Upstream-Version}) | clamav-data,
         dpkg (>= 1.16.1),
         logrotate,
         procps (>= 1:3.3.2),
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Description: anti-virus utility for Unix - sendmail integration
 Clam AntiVirus is an anti-virus toolkit for Unix. The main purpose of
 this software is the integration with mail servers (attachment
 scanning). The package provides a flexible and scalable
 multi-threaded daemon in the clamav-daemon package, a command-line
 scanner in the clamav package, and a tool for automatic updating via
 the Internet in the clamav-freshclam package. The programs are based
 on libclamav, which can be used by other software.
 .
 This package contains the ClamAV milter for use with sendmail. It can
 be configured to be run either standalone, or using clamav-daemon.
